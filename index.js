//Bài 1
function handleInBangSo() {
  var result = "";
  for (var i = 1; i < 100; i += 10) {
    for (var j = i; j < i + 10; j++) {
      result += j + " ";
    }
    result += "<br/>";
  }
  document.querySelector("#result-1").innerHTML = `<p>${result}</p>`;
}

//Bài 2
arrNum = [];
function handleNhapSo() {
  if (document.querySelector("#txt-day-so").value.trim() == "") {
    return;
  }
  var nhapSo = document.querySelector("#txt-day-so").value * 1;
  arrNum.push(nhapSo);
  document.querySelector("#txt-day-so").value == "";
  document.querySelector(
    "#result-them-so"
  ).innerHTML = `<p>Array : ${arrNum}</p>`;
}
//Kiểm tra số nguyên tố
function kiemTraSoNguyenTo(number) {
  var kiemTra = 1;
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i === 0) {
      kiemTra = 0;
      break;
    }
  }
  return kiemTra;
}
function handleInSoNguyenTo() {
  result = "";
  for (var index = 1; index < arrNum.length; index++) {
    var kiemTra = kiemTraSoNguyenTo(arrNum[index]);
    if (kiemTra) {
      result += arrNum[index] + " ";
    }
  }
  document.querySelector(
    "#result-2"
  ).innerHTML = `<p>Số Nguyên Tố : ${result}</p>`;
}

//Bài 3
function tinhSum(num) {
  var sumS = 0;
  for (var i = 2; i <= num; i++) {
    sumS += i;
  }
  sumS = sumS + 2 * (i - 1);
  return sumS;
}
function handleTinhTong() {
  var soN = document.querySelector("#txt-so-n").value * 1;
  var result = tinhSum(soN);
  document.querySelector("#result-3").innerHTML = `<p>Tổng : ${result}</p>`;
}

//Bài 4
function soSanh(a, b) {
  return b - a;
}

function handleSoLuongUocSo() {
  var arrDayUocSo = [];
  var soM = document.querySelector("#txt-so-m").value * 1;
  var soLuongUocSo = "";
  for (var j = soM; j >= 1; j--) {
    if (soM % j == 0) {
      soLuongUocSo += j + " ";
    }
  }
  arrDayUocSo.push(soLuongUocSo);
  arrDayUocSo.sort(soSanh);
  document.querySelector("#result-4").innerHTML = arrDayUocSo;
}

//Bài 5
function daoNguocSo(num) {
  num = num.toString();
  return num.split("").reverse().join("");
}
function handleSoDaoNguoc() {
  var soX = document.querySelector("#txt-so-x").value * 1;
  var result = daoNguocSo(soX);
  document.querySelector(
    "#result-5"
  ).innerHTML = `<p>Kết quả đảo ngược : ${result}</p>`;
}

//Bài 6
function handleSoNguyenDuongLonNhat() {
  var sum = 0;
  var result = 0;
  for (var i = 0; i <= 100; i++) {
    sum += i;
    result = i - 1;
    if (sum >= 100) {
      break;
    }
  }
  document.querySelector("#result-6").innerHTML = `<p>${result}</p>`;
}

//Bài 7
function handleBangCuuChuong() {
  var numValue = document.querySelector("#txt-num").value * 1;
  result = "";
  for (var i = 0; i <= 10; i++) {
    result += `${numValue} x ${i} = ${numValue * i} </br>`;
  }
  document.querySelector(
    "#result-7"
  ).innerHTML = `<p> Bảng cửu chương : <br/> ${result}</p>`;
}

//Bài 8
function handleChiaBai() {
  var arrPlayer = [[], [], [], []];
  var arrCard = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  var result = "";
  for (var i = 0; i < arrPlayer.length; i++) {
    for (var j = i; j < arrCard.length; j += 4) {
      result += arrPlayer[i].push(arrCard[j]);
    }
    result += "<br/>";
  }

  var show = "";
  for (var i = 0; i < arrPlayer.length; i++) {
    var laBai = `<p>Player ${i + 1} = ${arrPlayer[i]}</p>`;
    show += laBai;
  }

  document.querySelector("#result-8").innerHTML = `
        <p>${show}</p>
        `;
}
//Bài 9
function handleSoGaCho() {
  var soGaVaCho = document.querySelector("#txt-so-ga-cho").value * 1;
  var soChan = document.querySelector("#txt-so-chan").value*1;
  var chicken = 0;
  var dog = 0;
  for (var c = 0; c < soGaVaCho; c++) {
    for (var d = 0; d < soGaVaCho; d++) {
      if (c + d == soGaVaCho && c * 2 + d * 4 == soChan) {
        chicken += c;
        dog += d;
      }
    }
  }
  document.querySelector("#result-9").innerHTML=`
  <p>Số gà : ${chicken} con</p>
  <p>Số chó : ${dog} con</p>`
}

//Bài 10
function tinhGoc(gio, phut) {
  var gocPhut = phut * 6;
  var gocGio = gio * 30 + phut * 0.5;
  var gocLech = Math.abs(gocGio - gocPhut);
  return Math.min(gocLech, 360 - gocLech);
}

function handleGocLech() {
  var soGio = document.querySelector("#txt-so-gio").value * 1;
  var soPhut = document.querySelector("#txt-so-phut").value * 1;
  var ketQua = "";
  var gocLech = tinhGoc(soGio, soPhut);

  if (soGio >= 0 && soPhut >= 0) {
    ketQua = document.querySelector("#result-10").innerHTML = `
    <p>Góc lệch: ${gocLech} độ</p>
    `;
  } else {
    ketQua = document.querySelector("#result-10").innerHTML = `
    <p>Góc lệch: Giờ và phút không có số âm</p>
    `;
  }
}

